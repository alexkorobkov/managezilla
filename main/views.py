from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.conf import settings

from main.models import Task, get_start_current_week, ALL_ITEMS
from main.forms import TaskFilterForm, ReportFilterForm, PlanningFilterForm
from main.utils import get_tasks_board, get_planning_tasks, get_report, get_report_table


def sync(request):
    Task.objects.sync_data()
    return redirect('/')

def tasks_json_view(request):
    users = request.GET.getlist('users', [])
    statuses = request.GET.getlist('statuses', [ALL_ITEMS])
    types = request.GET.getlist('types', [ALL_ITEMS])
    milestones = request.GET.getlist('milestones', [])
    priorities = request.GET.getlist('priorities', [ALL_ITEMS])
    severities = request.GET.getlist('severities', [ALL_ITEMS])
    request.session['users'] = users
    request.session['statuses'] = statuses
    request.session['types'] = types
    request.session['milestones'] = milestones
    request.session['priorities'] = priorities
    request.session['severities'] = severities
    return JsonResponse(get_tasks_board(users, statuses, types, milestones, priorities, severities))


def tasks_view(request):
    search = request.GET.get('search', '')
    users = request.session.get('users', request.GET.getlist('users', []))
    statuses = request.session.get('statuses', request.GET.getlist('statuses', [ALL_ITEMS]))
    types = request.session.get('types', request.GET.getlist('types', [ALL_ITEMS]))
    milestones = request.session.get('milestones', request.GET.getlist('milestones', []))
    priorities = request.session.get('priorities', request.GET.getlist('priorities', []))
    severities = request.session.get('severities', request.GET.getlist('severities', []))
    context = {}
    context['task_filter_form'] = TaskFilterForm(request.GET or None, initial=dict(users=users, statuses=statuses, types=types, milestones=milestones, priorities=priorities, severities=severities))
    context['bugzilla_url'] = getattr(settings, 'BUGZILLA_URL', '') 
    return render(request, 'tasks_board.html', get_tasks_board(users, statuses, types, milestones, priorities, severities, search=search, context=context))


def planning_json_view(request):
    users = request.GET.getlist('p_users', [])
    statuses = request.GET.getlist('p_statuses', [ALL_ITEMS])
    types = request.GET.getlist('p_types', [ALL_ITEMS])
    milestones = request.GET.getlist('p_milestones', [])
    priorities = request.GET.getlist('p_priorities', [ALL_ITEMS])
    severities = request.GET.getlist('p_severities', [ALL_ITEMS])
    request.session['p_users'] = users
    request.session['p_statuses'] = statuses
    request.session['p_types'] = types
    request.session['p_milestones'] = milestones
    request.session['p_priorities'] = priorities
    request.session['p_severities'] = severities
    return JsonResponse(get_planning_tasks(users, statuses, types, milestones, priorities, severities))


def planning_view(request):
    search = request.GET.get('search', '')
    users = request.session.get('p_users', request.GET.getlist('p_users', []))
    statuses = request.session.get('p_statuses', request.GET.getlist('p_statuses', [ALL_ITEMS]))
    types = request.session.get('p_types', request.GET.getlist('p_types', [ALL_ITEMS]))
    milestones = request.session.get('p_milestones', request.GET.getlist('p_milestones', []))
    priorities = request.session.get('p_priorities', request.GET.getlist('p_priorities', []))
    severities = request.session.get('p_severities', request.GET.getlist('p_severities', []))
    context = {}
    context['planning_filter_form'] = PlanningFilterForm(request.GET or None, initial=dict(p_users=users, p_statuses=statuses, p_types=types, p_milestones=milestones, p_priorities=priorities, p_severities=severities))
    context['bugzilla_url'] = getattr(settings, 'BUGZILLA_URL', '') 
    return render(request, 'planning_board.html', get_planning_tasks(users, statuses, types, milestones, priorities, severities, search=search, context=context))


def report_json_view(request):
    report_users = request.GET.getlist('report_users', [])
    request.session['report_users'] = report_users
    return JsonResponse(get_report_table(report_users))


def report_view(request):
    context = {}
    search = request.GET.get('search', '')
    if request.GET.get('is_report', None) is not None:
        report_users = request.GET.get('report_users', '').split(',')
        last_change_time = str(get_start_current_week())
        tasks = Task.objects.get_tasks(last_change_time=last_change_time, users=report_users, statuses=[ALL_ITEMS], types=[ALL_ITEMS], priorities=[ALL_ITEMS], milestones=[ALL_ITEMS], severities=[ALL_ITEMS])
        return get_report(report_users, tasks)
    report_users = request.session.get('report_users', request.GET.getlist('report_users', []))
    context['bugzilla_url'] = getattr(settings, 'BUGZILLA_URL', '') 
    context['report_filter_form'] = ReportFilterForm(request.GET or None, initial=dict(report_users=report_users))
    return render(request, 'report.html', get_report_table(report_users, search=search, context=context))



