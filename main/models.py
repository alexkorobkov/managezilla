import xmlrpclib
import datetime
import random

from django.conf import settings
from django.db import models
from urlparse import urljoin


ALL_ITEMS = 'ALL'


def short_str(str_data, length):
    str_end = ''
    if len(str_data) > length:
        str_end = '...'
    return str_data[:length] + str_end


def get_current_year():
    today = datetime.date.today()
    return today.year


def get_current_week():
    today = datetime.date.today()
    return today.isocalendar()[1]


def get_start_current_week():
    today = datetime.date.today()
    start_week = today - datetime.timedelta(days=today.weekday())
    return start_week


class Milestone(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __unicode__(self):
        return "%s" % self.name


class Employee(models.Model):
    username = models.CharField(max_length=255, unique=True)
    color = models.CharField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return "%s" % self.username


class TaskManager(models.Manager):

    def _get_bugzilla_tasks(self, url, login, password, 
            last_change_time=None, 
            users=[], 
            statuses=[], 
            types=[], 
            milestones=[],
            priorities=[],
            severities=[]
            ):
        kwargs = dict()
        kwargs['Bugzilla_login'] = login
        kwargs['Bugzilla_password'] = password
        if last_change_time:
            kwargs['last_change_time'] = last_change_time
        if ALL_ITEMS not in statuses:
            kwargs['status'] = statuses
        if ALL_ITEMS not in types:
            kwargs['cf_tickettype'] = types
        if ALL_ITEMS not in users:
            kwargs['assigned_to'] = users
        if ALL_ITEMS not in milestones:
            kwargs['target_milestone'] = milestones
        if ALL_ITEMS not in priorities:
            kwargs['priority'] = priorities
        if ALL_ITEMS not in severities:
            kwargs['severity'] = severities
        bz = xmlrpclib.ServerProxy(urljoin(url, 'xmlrpc.cgi'))
        try:
            return bz.Bug.search(kwargs)['bugs']
        except Exception as e:
            print "Exception while retriving bugzilla!! {}".format(e)
            return []

    def sync_data(self):
        print "START sync"
        bg_url = getattr(settings, 'BUGZILLA_URL', '')
        bg_login = getattr(settings, 'BUGZILLA_LOGIN', '')
        bg_password = getattr(settings, 'BUGZILLA_PASSWORD', '')
        bg_tasks = self._get_bugzilla_tasks(
            bg_url,
            bg_login,
            bg_password,
            users=[ALL_ITEMS],
            statuses=[ALL_ITEMS],
            types=[ALL_ITEMS],
            milestones=[ALL_ITEMS],
            priorities=[ALL_ITEMS],
            severities=[ALL_ITEMS]
            )
        print "SYNC data"
        Milestone.objects.all().delete()
        Employee.objects.all().delete()
        milestones = []
        employees = []
        for bg_task in bg_tasks:
            if bg_task['target_milestone'] not in milestones:
                milestones.append(bg_task['target_milestone'])
            if bg_task['assigned_to'] not in employees:
                employees.append(bg_task['assigned_to'])
            if bg_task['creator'] not in employees:
                employees.append(bg_task['creator'])
        milestones_obj = []
        for m in milestones:
            milestones_obj.append(Milestone(name=m))
        employees_obj = []
        for e in employees:
            employees_obj.append(Employee(username=e, color="#%03x" % random.randint(0,0xFFF)))
        Milestone.objects.bulk_create(milestones_obj)
        Employee.objects.bulk_create(employees_obj)
        print "END sync"

    def get_bugzilla_users(self):
        return sorted(list(Employee.objects.all().values_list('username', flat=True)), key=lambda x: x)

    def get_bugzilla_milestones(self):
        return sorted(list(Milestone.objects.filter(name__icontains='>').values_list('name', flat=True)), key=lambda x: x.split('>')[1])

    def get_bugzilla_statuses(self):
        return getattr(settings, 'BUGZILLA_STATUSES', [])

    def get_bugzilla_types(self):
        return getattr(settings, 'BUGZILLA_TYPES', [])

    def get_bugzilla_priorities(self):
        return getattr(settings, 'BUGZILLA_PRIORITIES', [])

    def get_bugzilla_severities(self):
        return getattr(settings, 'BUGZILLA_SEVERITIES', [])

    def get_tasks(self, 
            last_change_time=None, 
            users=[], 
            statuses=[], 
            types=[], 
            milestones=[],
            priorities=[],
            severities=[]
            ):
        bg_url = getattr(settings, 'BUGZILLA_URL', '')
        bg_login = getattr(settings, 'BUGZILLA_LOGIN', '')
        bg_password = getattr(settings, 'BUGZILLA_PASSWORD', '')
        bg_tasks = self._get_bugzilla_tasks(
            bg_url,
            bg_login,
            bg_password,
            last_change_time=last_change_time,
            users=users,
            statuses=statuses,
            types=types,
            milestones=milestones,
            priorities=priorities,
            severities=severities
            )
        tasks = []
        for bg_task in bg_tasks:
            tasks.append(self.model(
                    task_id=bg_task['id'],
                    summary=bg_task['summary'],
                    assigned_to=bg_task['assigned_to'],
                    creator=bg_task['creator'],
                    priority=bg_task['priority'],
                    severity=bg_task['severity'],
                    task_type=bg_task['cf_tickettype'],
                    task_status=bg_task['status'],
                    task_resolution=bg_task['resolution'],
                    product=bg_task['product'],
                    component=bg_task['component'],
                    customer=bg_task['cf_customer'],
                    blocks=bg_task['blocks'],
                    depends_on=bg_task['depends_on'],
                    milestone=bg_task['target_milestone'],
                    creation_time=bg_task['creation_time'],
                    last_change_time=bg_task['last_change_time'],
                    actual_time=bg_task['actual_time'],
                    estimated_time=bg_task['estimated_time'],
                    remaining_time=bg_task['remaining_time'],
                ))
        return tasks


class Task(models.Model):
    task_id = models.PositiveIntegerField(unique=True, db_index=True)
    summary = models.CharField(max_length=255, null=True, blank=True)

    assigned_to = models.CharField(max_length=255, null=True, blank=True)
    creator = models.CharField(max_length=255, null=True, blank=True)

    priority = models.CharField(max_length=255, null=True, blank=True)
    severity = models.CharField(max_length=255, null=True, blank=True)

    task_type = models.CharField(max_length=255, null=True, blank=True)
    task_status = models.CharField(max_length=255, null=True, blank=True)
    task_resolution = models.CharField(max_length=255, null=True, blank=True)

    product = models.CharField(max_length=255, null=True, blank=True)
    component = models.CharField(max_length=255, null=True, blank=True)

    customer = models.CharField(max_length=255, null=True, blank=True)

    blocks = models.CharField(max_length=255, null=True, blank=True)
    depends_on = models.CharField(max_length=255, null=True, blank=True)

    milestone = models.CharField(max_length=255, null=True, blank=True)
    
    creation_time = models.DateTimeField(null=True, blank=True)
    last_change_time = models.DateTimeField(null=True, blank=True)

    actual_time = models.FloatField(default=0.0, null=True, blank=True)
    estimated_time = models.FloatField(default=0.0, null=True, blank=True)
    remaining_time = models.FloatField(default=0.0, null=True, blank=True)

    objects = TaskManager()

    @property
    def title(self):
        return self.subject

    @property
    def area(self):
        return self.product

    def get_tracker_url(self):
        bg_url = getattr(settings, 'BUGZILLA_URL', '')
        if bg_url:
            return bg_url + 'show_bug.cgi?id=%s' % self.task_id
        return '#'

    def is_closed(self):
        return self.task_status in getattr(settings, 'BUGZILLA_CLOSE_STATUSES', [])

    def get_short_summary(self):
        return short_str(self.summary, 60)

    def get_short_assigned_to(self):
        return short_str(self.assigned_to.split('@')[0], 20)

    def get_short_milestone(self):
        milestone = self.milestone
        if '>' in milestone:
            milestone = milestone.split('>')[1]
        return short_str(milestone, 6)

    def get_year(self):
        today = datetime.date.today()
        return today.year

    def get_week(self):
        return get_current_week()

    def get_u(self):
        start_week = get_start_current_week()
        today = datetime.date.today()
        if self.last_change_time >= start_week:
            return 'U'
        return ''

    def get_progress_status(self):
        if self.is_closed():
            return 'Done'
        else:
            return 'In progress...'

    def get_spent_hours(self):
        return self.actual_time

    def get_feature(self):
        is_feature = self.task_type in getattr(settings, 'BUGZILLA_FEATURE_TYPES', [])
        if is_feature:
            return 'F'
        return ''

    def get_color(self):
        e = Employee.objects.filter(username=self.assigned_to).first()
        if e:
            return e.color
        return 'gray'

