from django.contrib import admin

from main.models import Milestone, Employee, Task


admin.site.register(Milestone)
admin.site.register(Employee)
#admin.site.register(Task)