from django import forms

from main.models import Task, ALL_ITEMS


class TaskFilterForm(forms.Form):
    users = forms.MultipleChoiceField(choices=(), required=False)
    milestones = forms.MultipleChoiceField(choices=(), required=False)
    statuses = forms.MultipleChoiceField(choices=(), required=False)
    types = forms.MultipleChoiceField(choices=(), required=False)
    priorities = forms.MultipleChoiceField(choices=(), required=False)
    severities = forms.MultipleChoiceField(choices=(), required=False)

    def __init__(self, *args, **kwargs):
        super(TaskFilterForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['style'] = 'width: 100%;'
        self.fields['users'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_users() + [ALL_ITEMS]
            )
        self.fields['milestones'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_milestones() + [ALL_ITEMS]
            )
        self.fields['statuses'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_statuses() + [ALL_ITEMS]
            )
        self.fields['types'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_types() + [ALL_ITEMS]
            )
        self.fields['priorities'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_priorities() + [ALL_ITEMS]
            )
        self.fields['severities'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_severities() + [ALL_ITEMS]
            )


class PlanningFilterForm(forms.Form):
    p_users = forms.MultipleChoiceField(choices=(), required=False)
    p_milestones = forms.MultipleChoiceField(choices=(), required=False)
    p_statuses = forms.MultipleChoiceField(choices=(), required=False)
    p_types = forms.MultipleChoiceField(choices=(), required=False)
    p_priorities = forms.MultipleChoiceField(choices=(), required=False)
    p_severities = forms.MultipleChoiceField(choices=(), required=False)

    def __init__(self, *args, **kwargs):
        super(PlanningFilterForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['style'] = 'width: 100%;'
            self.fields[key].label = key[2:].capitalize()
        self.fields['p_users'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_users() + [ALL_ITEMS]
            )
        self.fields['p_milestones'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_milestones() + [ALL_ITEMS]
            )
        self.fields['p_statuses'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_statuses() + [ALL_ITEMS]
            )
        self.fields['p_types'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_types() + [ALL_ITEMS]
            )
        self.fields['p_priorities'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_priorities() + [ALL_ITEMS]
            )
        self.fields['p_severities'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_severities() + [ALL_ITEMS]
            )


class ReportFilterForm(forms.Form):
    report_users = forms.MultipleChoiceField(choices=(), required=False)

    def __init__(self, *args, **kwargs):
        super(ReportFilterForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['style'] = 'width: 100%;'
        self.fields['report_users'].choices = (
                (item, item) for item in Task.objects.get_bugzilla_users()
            )