$(document).ready(function () {
    /* enable strict mode */
    "use strict";

    // define redipsInit variable
    var redipsInit;

    // redips initialization
    redipsInit = function () {
        // reference to the REDIPS.drag library and message line
        var rd = REDIPS.drag,
            msg;
        // initialization
        rd.init();
        // set hover color for TD and TR
        rd.hover.colorTd = '#f5f5f5';
        rd.hover.colorTr = '#f5f5f5';
        // set hover border for current TD and TR
        rd.hover.borderTd = '2px solid #dddddd';
        rd.hover.borderTr = '2px solid #dddddd';
        // drop row after highlighted row (if row is dropped to other tables)
        rd.rowDropMode = 'after';

        // row was moved - event handler
        rd.event.rowMoved = function () {
            // set opacity for moved row
            // rd.obj is reference of cloned row (mini table)
            rd.rowOpacity(rd.obj, 85);
            // set opacity for source row and change source row background color
            // rd.objOld is reference of source row
            rd.rowOpacity(rd.objOld, 20, 'White');
        };
        // row was dropped to the source - event handler
        // mini table (cloned row) will be removed and source row should return to original state
        rd.event.rowDroppedSource = function () {
            // make source row completely visible (no opacity)
            rd.rowOpacity(rd.objOld, 100);
        };

        rd.event.dropped = function(targetCell) {
            
        };


        rd.event.rowDropped = function(targetRow, sourceTable, sourceRowIndex){

        };
    };
    // add onload event listener
    // if (window.addEventListener) {
    //     window.addEventListener('load', redipsInit, false);
    // }
    // else if (window.attachEvent) {
    //     window.attachEvent('onload', redipsInit);
    // }
});