import xlwt
import datetime

from django.http import HttpResponse
from django.template.loader import render_to_string
from django.conf import settings

from main.models import Task, get_start_current_week, get_current_week, ALL_ITEMS


def get_report(users, tasks):
    today = datetime.date.today()
    year = today.year
    week = get_current_week()
    user_str = 'team'
    if len(users) == 1:
        user_str = users[0].split('@')[0]

    tasks = sorted(tasks, key=lambda x: x.assigned_to)

    start_week = get_start_current_week()
    filename = "%s_weeklyreport_%s_w%s.xls" % (user_str, year, week)

    wbk = xlwt.Workbook()
    style = xlwt.XFStyle()
    style.num_format_str = "@"
    hl_sheet = wbk.add_sheet('Highlights')
    style = xlwt.easyxf('font: color green, bold True;')
    hl_sheet.write(0, 0, 'Green Flags', style)
    style = xlwt.easyxf('font: color red, bold True;')
    hl_sheet.write(3, 0, 'Red Flags', style)
    style = xlwt.easyxf('font: color brown, bold True;')
    hl_sheet.write(6, 0, 'On Site', style)
    hl_sheet.write(9, 0, 'Vacations', style)

    style = xlwt.XFStyle()
    style.num_format_str = "@"
    sheet = wbk.add_sheet('Weekly update')
    headers = (
        ('Year', (None, year)),
        ('Week', (None, week)),
        ('Assigned to Full name', ('assigned_to', None)), 
        ('Area', ('product', None)),
        ('U', (None, '')),
        ('Spent Hours', (None, 0)),
        ('Progress Status', (None, '')), 
        ('Feature', (None, '')),
        ('Bug ID', ('task_id', None)),
        ('Ticket Type', ('task_type', None)),
        ('Status', ('task_status', None)),
        ('Title', ('summary', None)),
        ('Priority', ('priority', None)),
        ('Severity', ('severity', None)),
        ('Customer', ('customer', ''))
        )
    col_num = 0
    row_num = 0
    style = xlwt.easyxf('font: bold True;')
    for header in headers:
        sheet.write(row_num, col_num, header[0], style)
        col_num += 1
    style = xlwt.easyxf('font: bold False;')
    row_num = 1
    for task in tasks:
        col_num = 0
        for header in headers:
            if header[1][0] is not None:
                sheet.write(row_num, col_num, getattr(task, header[1][0], None) or header[1][1], style)
            else:
                data = header[1][1]
                if header[0]== 'U':
                    data = task.get_u()
                if header[0]== 'Spent Hours':
                    data = task.get_spent_hours()
                if header[0]== 'Progress Status':
                    data = task.get_progress_status()
                sheet.write(row_num, col_num, data, style)
            col_num += 1
        row_num += 1
    response = HttpResponse(content_type="application/vnd.ms-excel")
    response['Content-Disposition'] = 'attachment; filename=%s.xls' % filename
    wbk.save(response)
    return response


def search_task(task, search):
    if str(task.task_id).lower() in search.lower() or search.lower() in str(task.task_id).lower():
        return True
    if str(task.summary).lower() in search.lower() or search.lower() in str(task.summary).lower():
        return True
    return False


def get_tasks_board(users, statuses, types, milestones, priorities, severities, search='', context={}):
    if ALL_ITEMS in statuses:
        statuses = Task.objects.get_bugzilla_statuses()
    if ALL_ITEMS in types:
        types = Task.objects.get_bugzilla_types()
    tasks = Task.objects.get_tasks(users=users, statuses=statuses, types=types, milestones=milestones, priorities=priorities, severities=severities)
    if search:
        tasks= filter(lambda x: search_task(x, search), tasks)
    context['data_content'] = render_to_string('tasks.html', dict(
        tasks=tasks, 
        statuses=statuses, 
        types=types, 
        )
    )
    return context

def get_planning_tasks(users, statuses, types, milestones, priorities, severities, search='', context={}):
    if ALL_ITEMS in statuses:
        statuses = Task.objects.get_bugzilla_statuses()
    if ALL_ITEMS in types:
        types = Task.objects.get_bugzilla_types()
    unassigned_milestone = getattr(settings, 'UNASSIGNED_MILESTONE', '') 
    backlog_statuses = filter(lambda x: x not in getattr(settings, 'BUGZILLA_CLOSE_STATUSES', '') , statuses)
    backlog_tasks = Task.objects.get_tasks(users=users, statuses=backlog_statuses, types=types, milestones=[unassigned_milestone], priorities=priorities, severities=severities)
    tasks = Task.objects.get_tasks(users=users, statuses=statuses, types=types, milestones=milestones, priorities=priorities, severities=severities)
    if search:
        tasks= filter(lambda x: search_task(x, search), tasks)
    context['data_content'] = render_to_string('planning_tasks.html', dict(
        backlog_tasks=backlog_tasks,
        milestones=milestones,
        tasks=tasks, 
        statuses=statuses, 
        types=types, 
        bugzilla_url=getattr(settings, 'BUGZILLA_URL', '') 
        )
    )
    return context

def get_report_table(users, search='', context={}):
    last_change_time = str(get_start_current_week())
    tasks = Task.objects.get_tasks(last_change_time=last_change_time, users=users, statuses=[ALL_ITEMS], types=[ALL_ITEMS], priorities=[ALL_ITEMS], milestones=[ALL_ITEMS], severities=[ALL_ITEMS])
    if search:
        tasks= filter(lambda x: search_task(x, search), tasks)
    tasks = sorted(tasks, key=lambda x: x.assigned_to)
    context['data_content'] = render_to_string('report_table.html', dict(
        tasks=tasks, 
        )
    )
    return context
