import hashlib
from django import template

register = template.Library()


@register.filter(name='check_status')
def check_status(task, task_status):
    return task.task_status == task_status


@register.filter(name='check_type')
def check_type(task, task_type):
    return task.task_type == task_type

@register.filter(name='filter_milestone')
def filter_milestone(tasks, milestone):
    return filter(lambda x: x.milestone == milestone, tasks)


@register.filter(name='filter_milestone')
def filter_milestone(tasks, milestone):
    return filter(lambda x: x.milestone == milestone, tasks)


@register.filter(name='get_hash')
def get_hash(str_data):
    hash_object = hashlib.md5(str_data)
    return hash_object.hexdigest()


@register.filter(name='get_milestone_time')
def get_milestone_time(tasks, milestone):
    tasks = filter_milestone(tasks, milestone)
    return get_time(tasks)


@register.filter(name='get_time')
def get_time(tasks):
    estimated_time = [task.estimated_time for task in tasks]
    actual_time = [task.actual_time for task in tasks]
    return "%s(%s)" % (sum(estimated_time), sum(actual_time))


@register.filter(name='tasks_table_col_width')
def tasks_table_col_width(statuses):
    count = len(statuses) + 1
    persent = 100 / count
    return "%s" % persent
