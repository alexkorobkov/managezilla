# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(unique=True, max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Milestone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('task_id', models.PositiveIntegerField(unique=True, db_index=True)),
                ('summary', models.CharField(max_length=255, null=True, blank=True)),
                ('assigned_to', models.CharField(max_length=255, null=True, blank=True)),
                ('creator', models.CharField(max_length=255, null=True, blank=True)),
                ('priority', models.CharField(max_length=255, null=True, blank=True)),
                ('severity', models.CharField(max_length=255, null=True, blank=True)),
                ('task_type', models.CharField(max_length=255, null=True, blank=True)),
                ('task_status', models.CharField(max_length=255, null=True, blank=True)),
                ('task_resolution', models.CharField(max_length=255, null=True, blank=True)),
                ('product', models.CharField(max_length=255, null=True, blank=True)),
                ('component', models.CharField(max_length=255, null=True, blank=True)),
                ('customer', models.CharField(max_length=255, null=True, blank=True)),
                ('blocks', models.CharField(max_length=255, null=True, blank=True)),
                ('depends_on', models.CharField(max_length=255, null=True, blank=True)),
                ('milestone', models.CharField(max_length=255, null=True, blank=True)),
                ('creation_time', models.DateTimeField(null=True, blank=True)),
                ('last_change_time', models.DateTimeField(null=True, blank=True)),
                ('actual_time', models.FloatField(default=0.0, null=True, blank=True)),
                ('estimated_time', models.FloatField(default=0.0, null=True, blank=True)),
                ('remaining_time', models.FloatField(default=0.0, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
