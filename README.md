Managezilla
=========
## Screenshot
![Screenshot](https://raw.github.com/vsdudakov/managezilla/master/screenshots/1.png)  
![Screenshot](https://raw.github.com/vsdudakov/managezilla/master/screenshots/2.png)  
![Screenshot](https://raw.github.com/vsdudakov/managezilla/master/screenshots/3.png)  

## Description
There is a task/planning (SCRUM) board for bugzilla.  
Now it is readonly mode. You can not change data of tasks using this board (may be in the future).  
Anyway any suggestions are welcome!


## First steps 
Powered by django framework

1. install python-virtualenv (sudo apt-get install python-virtualenv && virtualenv your_virtual_env)
1. source your_virtual_env/bin/activate
1. install dependencies to virtualenv (pip install -r requirements.txt)
1. create settings.py file (cp managezilla/settings.py.tmpl managezilla/settings.py)
1. cusomize some parameters in your settings (managezilla/settings.py) (see Parameters section)
1. run django test server (python manage.py runserver)
1. create superuser (python manage.py createsuperuser)
1. sync sqlite db (python manage.py migrate)
1. go to 127.0.0.1:8000 in your browser
1. sync some data from bugzilla 127.0.0.1:800/sync/



## Parameters

_BUGZILLA_URL = 'https://bugzilla.yourcompany.com/'_  
Setup your root path to bugzilla  

_UGZILLA_LOGIN = ''_  
Setup your bugzilla login  

_BUGZILLA_PASSWORD = ''_   
Setup your bugzilla password  

_BUGZILLA_STATUSES = ['CONFIRMED', 'ASSIGNED', 'REOPENED', 'RESOLVED', 'VERIFIED_ON_BRANCH', 'VERIFIED']_  
Setup your all bugzilla statuses  

_BUGZILLA_CLOSE_STATUSES = ['RESOLVED', 'VERIFIED_ON_BRANCH', 'VERIFIED']_  
Setup your all bugzilla close statuses  

_BUGZILLA_TYPES = ['Defect', 'Enhancement', 'Task', 'Feature', 'Substory']_  
Setup your all bugzilla types  

_BUGZILLA_FEATURE_TYPES = ['Feature', 'Substory']_  
Setup feature types  

_BUGZILLA_PRIORITIES = ['P1-ASAP', 'P2-High', 'P3-Medium', 'P4-Low', 'Undefined']_  
Setup your all bugzilla priorities  

_BUGZILLA_SEVERITIES = ['S1-Critical', 'S2-High', 'S3-Medium', 'S4-Low']_  
Setup your all bugzilla severities  

_UNASSIGNED_MILESTONE = '---'_  
Setup unassigned milestone (for filtering tasks for backlog)  


## Contact

[Vsevolod Dudakov](http://github.com/vsdudakov)(vsdudakov@gmail.com)

## Licence

Managezilla is available under the MIT licence. See the LICENCE file for more info.
