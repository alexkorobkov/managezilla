from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'main.views.report_view', name='report'),
    url(r'^tasks/$', 'main.views.tasks_view', name='tasks-board'),
    url(r'^planning/$', 'main.views.planning_view', name='planning-board'),
    
    url(r'^report/json/$', 'main.views.report_json_view', name='report-json'),
    url(r'^tasks/json/$', 'main.views.tasks_json_view', name='tasks-json'),
    url(r'^planning/json/$', 'main.views.planning_json_view', name='planning-json'),

    url(r'^sync/$', 'main.views.sync', name='sync'),
    url(r'^admin/', include(admin.site.urls)),
)
